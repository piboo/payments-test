package com.payment.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.payment.dto.ChargesDTO;
import com.payment.dto.FxDTO;
import com.payment.dto.PaymentDTO;
import com.payment.dto.PaymentPartyDTO;
import com.payment.dto.request.valueObject.Amount;
import com.payment.exception.PaymentsWrongParameterException;
import java.io.IOException;

public class PaymentDTODeserializer extends StdDeserializer<PaymentDTO> {

        public PaymentDTODeserializer() {
            this(null);
        }

        public PaymentDTODeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public PaymentDTO deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
            JsonNode node = jp.getCodec().readTree(jp);

            String amount = node.get("amount").asText();
            String currency = node.get("currency").asText();

            try {
                return new PaymentDTO(
                    this.extractPaymentParty(node.get("beneficiary_party")),
                    this.extractPaymentParty(node.get("debtor_party")),
                    new ChargesDTO(),
                    new FxDTO(),
                    new Amount(amount, currency)
                );
            } catch (PaymentsWrongParameterException e) {
                throw new IOException(
                    "Impossible to deserialize object : " +
                        e.getMessage()
                );
            }
        }

        private PaymentPartyDTO extractPaymentParty(JsonNode node) {
            return new PaymentPartyDTO(
                node.get("account_name").asText(),
                node.get("account_number").asText(),
                node.get("account_number_code").asText(),
                node.get("address").asText(),
                node.get("bank_id").asText(),
                node.get("bank_id_code").asText(),
                node.get("name").asText()
            );
        }
}


