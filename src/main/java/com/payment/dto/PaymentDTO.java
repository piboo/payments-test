package com.payment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.payment.dto.request.valueObject.Amount;
import com.payment.serializer.PaymentDTODeserializer;
import org.hibernate.type.CustomType;

import java.io.Serializable;

@JsonDeserialize(using = PaymentDTODeserializer.class)
public class PaymentDTO implements Serializable {
    private String amount;
    private String currency;
    @JsonProperty("beneficiary_party")
    private PaymentPartyDTO beneficiaryParty;
    @JsonProperty("debtor_party")
    private PaymentPartyDTO debtorParty;
    @JsonProperty("charges")
    private ChargesDTO chargesDTO;
    @JsonProperty("fx")
    private FxDTO fxDTO;
    @JsonProperty("numeric_reference")
    private String numericReference = "1002001";
    @JsonProperty("payment_id")
    private String paymentID = "123456789012345678";
    @JsonProperty("payment_purpose")
    private String paymentPurpose = "Paying for goods/services";
    @JsonProperty("payment_scheme")
    private String paymentScheme = "FPS";
    @JsonProperty("payment_type")
    private String paymentType = "Credit";
    @JsonProperty("scheme_payment_sub_type")
    private String schemePaymentSubType = "InternetBanking";
    @JsonProperty("scheme_payment_type")
    private String schemePaymentType = "ImmediatePayment";

    public PaymentDTO(
        PaymentPartyDTO beneficiaryParty,
        PaymentPartyDTO debtorParty,
        ChargesDTO chargesDTO,
        FxDTO fxDTO,
        Amount amount
    ) {
        this.amount = Float.toString(amount.getAmount());
        this.currency = amount.getCurrency();
        this.beneficiaryParty = beneficiaryParty;
        this.debtorParty = debtorParty;
        this.chargesDTO = chargesDTO;
        this.fxDTO = fxDTO;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public PaymentPartyDTO getBeneficiaryParty() {
        return beneficiaryParty;
    }

    public PaymentPartyDTO getDebtorParty() {
        return debtorParty;
    }

    public ChargesDTO getChargesDTO() {
        return chargesDTO;
    }

    public FxDTO getFxDTO() {
        return fxDTO;
    }

    public String getNumericReference() {
        return numericReference;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public String getPaymentPurpose() {
        return paymentPurpose;
    }

    public String getPaymentScheme() {
        return paymentScheme;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getSchemePaymentSubType() {
        return schemePaymentSubType;
    }

    public String getSchemePaymentType() {
        return schemePaymentType;
    }
}
