package com.payment.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PaymentPartyDTO implements Serializable {
    @JsonProperty("account_name")
    String accountName;
    @JsonProperty("account_number")
    String accountNumber;
    @JsonProperty("account_number_code")
    String accountNumberCode;
    @JsonProperty("address")
    String address;
    @JsonProperty("bank_id")
    String bankId;
    @JsonProperty("bank_id_code")
    String bankIdCode;
    @JsonProperty("name")
    String name;


    public PaymentPartyDTO(
        String accountName,
        String accountNumber,
        String accountNumberCode,
        String address,
        String bankId,
        String bankIdCode,
        String name
    ) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.accountNumberCode = accountNumberCode;
        this.address = address;
        this.bankId = bankId;
        this.bankIdCode = bankIdCode;
        this.name = name;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountNumberCode() {
        return accountNumberCode;
    }

    public String getAddress() {
        return address;
    }

    public String getBankId() {
        return bankId;
    }

    public String getBankIdCode() {
        return bankIdCode;
    }

    public String getName() {
        return name;
    }
}
