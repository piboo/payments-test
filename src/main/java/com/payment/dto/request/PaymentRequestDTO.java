package com.payment.dto.request;


import com.payment.dto.request.valueObject.Account;
import com.payment.dto.request.valueObject.Amount;
import com.payment.exception.PaymentsWrongParameterException;

public class PaymentRequestDTO {
    private Amount amount;
    private String reference;
    private String referenceE2E;
    private String paymentType;
    private Account debtorAccount;
    private Account beneficiaryAccount;

    PaymentRequestDTO(
        Amount amount,
        Account debtorAccount,
        Account beneficiaryAccount,
        String reference,
        String referenceE2E,
        String paymentType
    ) throws PaymentsWrongParameterException {

        this.amount = amount;
        this.debtorAccount = debtorAccount;
        this.beneficiaryAccount = beneficiaryAccount;
        this.reference = reference;
        this.referenceE2E = referenceE2E;
        this.paymentType = paymentType;


        this.validate();
    }

    private void validate() throws PaymentsWrongParameterException {

        if (this.reference.isEmpty()) {
            throw new PaymentsWrongParameterException("reference can not be empty");
        }

        if (this.referenceE2E.isEmpty()) {
            throw new PaymentsWrongParameterException("referenceE2E can not be empty");
        }

        if (this.paymentType.isEmpty()) {
            throw new PaymentsWrongParameterException("paymentType can not be empty");
        }
    }

    public Amount getAmount() {
        return amount;
    }

    public String getReference() {
        return reference;
    }

    public String getReferenceE2E() {
        return referenceE2E;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public Account getDebtorAccount() {
        return debtorAccount;
    }

    public Account getBeneficiaryAccount() {
        return beneficiaryAccount;
    }
}
