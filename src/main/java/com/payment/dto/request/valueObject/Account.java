package com.payment.dto.request.valueObject;

import com.payment.exception.PaymentsWrongParameterException;

public class Account {
    private String accountName;
    private String accountNumber;
    private String accountNumberCode;

    public Account(String accountName, String accountNumber, String accountNumberCode) throws PaymentsWrongParameterException {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.accountNumberCode = accountNumberCode;

        this.validate();
    }


    private void validate() throws PaymentsWrongParameterException {
        if (this.accountName == null || this.accountName.isEmpty()) {
            throw new PaymentsWrongParameterException("Account Name is empty");
        }

        if (this.accountNumber == null || this.accountNumber.length() < 22) {
            throw new PaymentsWrongParameterException("Account Name needs to be 22 character");
        }

        if (this.accountNumberCode == null || !this.accountNumberCode.equals("IBAN")) {
            throw new PaymentsWrongParameterException("Only IBAN Account Number accepted");
        }
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountNumberCode() {
        return accountNumberCode;
    }
}
