package com.payment.dto.request.valueObject;

import com.payment.exception.PaymentsWrongParameterException;

public class Amount {
    private float amount;
    private String currency;

    public Amount(String amount, String currency) throws PaymentsWrongParameterException {
        if (amount == null) {
            throw new PaymentsWrongParameterException("Amount not null");
        }
        if (currency == null) {
            throw new PaymentsWrongParameterException("Currency not null");
        }

        this.amount = Float.parseFloat(amount);
        this.currency = currency;

        this.validate();
    }

    private void validate() throws PaymentsWrongParameterException  {
        if (this.amount<0) {
            throw new PaymentsWrongParameterException("Amount not acceptable less then 0");
        }

        if (this.currency.length() != 3) {
            throw new PaymentsWrongParameterException("Wrong Currency");
        }
    }

    public float getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}