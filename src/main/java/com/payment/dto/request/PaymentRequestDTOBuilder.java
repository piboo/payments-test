package com.payment.dto.request;

import com.payment.dto.request.valueObject.Account;
import com.payment.dto.request.valueObject.Amount;
import com.payment.exception.PaymentsWrongParameterException;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PaymentRequestDTOBuilder {

    private String[] mandatoryParameterArray = new String[]{
        "amount","currency","beneficiary_account","debtor_account","reference",
        "end_to_end","payment_type"
    };

    public PaymentRequestDTO build(Map<String, Object> requestMap) throws PaymentsWrongParameterException {
        Account beneficiaryAccount, debtorAccount;
        Map<?,?> beneficiaryAccountRequest,debtorAccountRequest;

        for (String mandatoryParameter: mandatoryParameterArray
             ) {
            if (!requestMap.containsKey(mandatoryParameter) || requestMap.get(mandatoryParameter)== null) {
                throw new PaymentsWrongParameterException("Request don't contain all the field "+mandatoryParameter +" missing");
            }
        }

        Amount amount = new Amount(
            requestMap.get("amount").toString(),
            requestMap.get("currency").toString()
        );

        if (!(requestMap.get("debtor_account") instanceof Map)) {
            throw new PaymentsWrongParameterException("Beneficiary Account is null");
        }
        beneficiaryAccountRequest = (Map<?,?>) requestMap.get("beneficiary_account");


        if (!(requestMap.get("debtor_account") instanceof Map)) {
            throw new PaymentsWrongParameterException("Beneficiary Account is null");
        }
        debtorAccountRequest = (Map<?,?>) requestMap.get("debtor_account");


        try {
            beneficiaryAccount = new Account(
                (String)beneficiaryAccountRequest.get("account_name"),
                (String)beneficiaryAccountRequest.get("account_number"),
                (String)beneficiaryAccountRequest.get("account_number_code")
                );
        } catch (PaymentsWrongParameterException exception) {
            throw new PaymentsWrongParameterException("Beneficiary Account:" + exception.getMessage());
        }

        try {
            debtorAccount = new Account(
                (String)debtorAccountRequest.get("account_name"),
                (String)debtorAccountRequest.get("account_number"),
                (String)debtorAccountRequest.get("account_number_code")
            );
        } catch (PaymentsWrongParameterException exception) {
            throw new PaymentsWrongParameterException("Debtor Account:" + exception.getMessage());
        }

        return new PaymentRequestDTO(
            amount,
            debtorAccount,
            beneficiaryAccount,
            requestMap.get("reference")!= null? requestMap.get("reference").toString():"",
            requestMap.get("end_to_end")!= null? requestMap.get("end_to_end").toString():"",
            requestMap.get("payment_type")!= null? requestMap.get("payment_type").toString():""
        );
    }

}
