package com.payment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChargesDTO implements Serializable {
    @JsonProperty("bearer_code")
    String bearerCode = "bearer_code";
    @JsonProperty("receiver_charges_amount")
    String receiver_charges_amount = "1.00";
    @JsonProperty("receiver_charges_currency")
    String receiver_charges_currency = "USD";

    @JsonProperty("sender_charges")
    List<Map> senderCharges;

    public ChargesDTO(String bearerCode, String receiver_charges_amount, String receiver_charges_currency, List<Map> senderCharges) {
        this.bearerCode = bearerCode;
        this.receiver_charges_amount = receiver_charges_amount;
        this.receiver_charges_currency = receiver_charges_currency;
        this.senderCharges = senderCharges;
    }

    public ChargesDTO() {
        senderCharges= new ArrayList<>();
        Map<String,String> map = new HashMap<>();
        map.put("amount", "5.00");
        map.put("currency", "GBP");
        senderCharges.add(map);
        Map<String,String> map2 = new HashMap<>();
        map2.put("amount", "10.00");
        map2.put("currency", "USD");
        senderCharges.add(map2);
    }

}
