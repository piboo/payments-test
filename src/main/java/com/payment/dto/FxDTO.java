package com.payment.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FxDTO implements Serializable {
    @JsonProperty("contract_reference")
    private String contractReference = "FX123";
    @JsonProperty("exchange_rate")
    private String exchange_rate = "2.00000";
    @JsonProperty("original_amount")
    private String original_amount = "200.42";
    @JsonProperty("original_currency")
    private String original_currency = "GBP";

    public FxDTO(String contractReference, String exchange_rate, String original_amount, String original_currency) {
        this.contractReference = contractReference;
        this.exchange_rate = exchange_rate;
        this.original_amount = original_amount;
        this.original_currency = original_currency;
    }

    public FxDTO() {
    }

    public String getContractReference() {
        return contractReference;
    }

    public String getExchange_rate() {
        return exchange_rate;
    }

    public String getOriginal_amount() {
        return original_amount;
    }

    public String getOriginal_currency() {
        return original_currency;
    }
}
