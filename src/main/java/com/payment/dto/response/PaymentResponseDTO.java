package com.payment.dto.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.payment.dto.PaymentDTO;

import java.io.Serializable;

public class PaymentResponseDTO implements Serializable{
    @JsonProperty("properties")
    PaymentDTO paymentDTO;

    @JsonProperty("version")
    String version;

    @JsonProperty("uuid")
    String uuid;

    public PaymentResponseDTO(PaymentDTO paymentDTO, String uuid) {
        this.paymentDTO = paymentDTO;
        this.version = "0";
        this.uuid = uuid;
    }
}
