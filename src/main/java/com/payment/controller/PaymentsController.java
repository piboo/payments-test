package com.payment.controller;


import com.payment.dataMapper.PaymentDataMapper;
import com.payment.dto.PaymentDTO;
import com.payment.dto.request.PaymentRequestDTO;
import com.payment.dto.request.PaymentRequestDTOBuilder;
import com.payment.dto.response.PaymentResponseDTO;
import com.payment.exception.PaymentsSerializerException;
import com.payment.exception.PaymentsWrongParameterException;
import com.payment.service.PaymentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping(path="/payments")
public class PaymentsController {

    private PaymentRequestDTOBuilder paymentRequestDTOBuilder;
    private PaymentMapper paymentMapper;
    private PaymentDataMapper paymentDataMapper;

    @Autowired
    public PaymentsController(
        PaymentRequestDTOBuilder paymentRequestDTOBuilder,
        PaymentMapper paymentMapper,
        PaymentDataMapper paymentDataMapper
    ) {
        this.paymentRequestDTOBuilder = paymentRequestDTOBuilder;
        this.paymentMapper = paymentMapper;
        this.paymentDataMapper = paymentDataMapper;
    }

    @PostMapping()
    public @ResponseBody ResponseEntity createPayment(@RequestBody Map<String, Object> paymentRequestMap) {
        PaymentRequestDTO paymentRequestDTO;
        ResponseEntity responseEntity;

        try {
            paymentRequestDTO = paymentRequestDTOBuilder.build(paymentRequestMap);

        } catch (PaymentsWrongParameterException exception) {
            return new ResponseEntity<>(
                exception.getMessage(),
                HttpStatus.BAD_REQUEST
            );
        }

        PaymentDTO paymentDTO = paymentMapper.mapPayment(paymentRequestDTO);

        if (paymentDataMapper.savePayment(paymentDTO)) {
            responseEntity = new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }


    @GetMapping()
    public @ResponseBody ResponseEntity getPayments() {
        try {
            return new ResponseEntity<>(paymentDataMapper.getAllPayments(), HttpStatus.OK);
        } catch (PaymentsSerializerException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{uuid}")
    public @ResponseBody ResponseEntity getOnePayments(
        @PathVariable("uuid") String uuid) {
        PaymentResponseDTO paymentResponseDTO;

        try {
            paymentResponseDTO = paymentDataMapper.getOnePayment(uuid);
        } catch (PaymentsSerializerException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(paymentResponseDTO == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(paymentResponseDTO,HttpStatus.OK);
    }


}
