package com.payment.repository;

import com.payment.entity.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface PaymentRepository extends CrudRepository<Payment, Integer>  {
    Payment findOneByUuid(String uuid);
}
