package com.payment.service;

import com.payment.dto.ChargesDTO;
import com.payment.dto.FxDTO;
import com.payment.dto.PaymentDTO;
import com.payment.dto.PaymentPartyDTO;
import com.payment.dto.request.PaymentRequestDTO;
import org.springframework.stereotype.Service;

@Service
public class PaymentMapper {

    public PaymentDTO mapPayment(PaymentRequestDTO paymentRequestDTO) {

        return new PaymentDTO(
            new PaymentPartyDTO(
                paymentRequestDTO.getBeneficiaryAccount().getAccountName(),
                paymentRequestDTO.getBeneficiaryAccount().getAccountNumber(),
                paymentRequestDTO.getBeneficiaryAccount().getAccountNumberCode(),
                "Beneficiary Bank Address",
                "122111",
                "ABC12121",
                "Ben Eficiary"
            ),
            new PaymentPartyDTO(
                paymentRequestDTO.getDebtorAccount().getAccountName(),
                paymentRequestDTO.getDebtorAccount().getAccountNumber(),
                "BBAN",
                "Debtor Bank Address",
                "122111",
                "ABC12121",
                "Ben Eficiary"
            ),
            new ChargesDTO(),
            new FxDTO(),
            paymentRequestDTO.getAmount()
        );

    }
}
