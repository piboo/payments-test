package com.payment.dataMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payment.dto.PaymentDTO;
import com.payment.dto.response.PaymentResponseDTO;
import com.payment.entity.Payment;
import com.payment.exception.PaymentsSerializerException;
import com.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PaymentDataMapper {

    private PaymentRepository paymentRepository;
    private ObjectMapper objectMapper;

    @Autowired
    public PaymentDataMapper (
        PaymentRepository paymentRepository,
        ObjectMapper objectMapper
    ) {
        this.paymentRepository = paymentRepository;
        this.objectMapper = objectMapper;
    }

    public boolean savePayment(PaymentDTO paymentDTO) {
        Payment payment = new Payment();

        payment.setUuid(
            UUID.randomUUID().toString()
        );

        try {
            payment.setPayment(
                objectMapper.writeValueAsString(paymentDTO)
            );

        } catch (JsonProcessingException exception) {
            System.out.print(exception.getMessage());
            return false;
        }

        paymentRepository.save(payment);

        return true;
    }

    public List<PaymentResponseDTO> getAllPayments() throws PaymentsSerializerException {
        Iterable<Payment> payments = paymentRepository.findAll();

        List<PaymentResponseDTO> paymentResponseDTOS = new ArrayList<>();
        objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);

        try {
            for (Payment payment : payments) {
                paymentResponseDTOS.add(
                    new PaymentResponseDTO(
                        objectMapper.readValue(payment.getPayment(), PaymentDTO.class),
                        payment.getUuid()
                    )
                );
            }
        } catch (IOException e) {
            throw new PaymentsSerializerException("Deserializing issues");
        }

        return paymentResponseDTOS;
    }

    public PaymentResponseDTO getOnePayment(String uuid) throws PaymentsSerializerException {
        PaymentResponseDTO paymentResponseDTO = null;

        Payment payment  = paymentRepository.findOneByUuid(uuid);

        if (payment == null) {
            return null;
        }

        objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);

        try {
            paymentResponseDTO = new PaymentResponseDTO(
                objectMapper.readValue(payment.getPayment(), PaymentDTO.class),
                payment.getUuid()
            );
        } catch(IOException exception) {
            throw new PaymentsSerializerException("Deserializing issues");
        }

        return paymentResponseDTO;
    }

}
