package com.payment.exception;


public class PaymentsWrongParameterException extends Exception {

    public PaymentsWrongParameterException(String message) {
        super(message);
    }
}
