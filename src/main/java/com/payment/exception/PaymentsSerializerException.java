package com.payment.exception;


public class PaymentsSerializerException extends Exception {

    public PaymentsSerializerException(String message) {
        super(message);
    }
}
