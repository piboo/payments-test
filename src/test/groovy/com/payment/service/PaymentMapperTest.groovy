package com.payment.dto.service

import com.payment.dto.PaymentDTO
import com.payment.dto.request.PaymentRequestDTO
import com.payment.dto.request.valueObject.Account
import com.payment.dto.request.valueObject.Amount
import com.payment.service.PaymentMapper
import spock.lang.Specification

class PaymentMapperTest extends Specification {
    PaymentRequestDTO paymentRequestDTO
    Account account
    Amount amount

    def setup() {
        paymentRequestDTO = Mock(PaymentRequestDTO)
        account = Mock(Account)
        account.getAccountNumberCode()>> "numberCode"
        account.getAccountNumber()>> "11211211"
        account.getAccountName()>> "name"

        amount = Mock(Amount)
        amount.getAmount()>>11.0
        amount.getCurrency()>> "GBP"
    }

    def "Map correctly a request"() {
        when: 'a DTO is passed'
        def paymentMapper = new PaymentMapper()

        paymentRequestDTO.getReference()>> "reference"
        paymentRequestDTO.getReferenceE2E()>> "reference-e2e"
        paymentRequestDTO.getPaymentType()>> "type"
        paymentRequestDTO.getBeneficiaryAccount()>> account
        paymentRequestDTO.getDebtorAccount()>> account
        paymentRequestDTO.getAmount() >> amount

        def paymentDTO = paymentMapper.mapPayment(paymentRequestDTO)

        then:
        paymentDTO instanceof PaymentDTO
    }
}

