package com.payment.controller

import com.payment.dataMapper.PaymentDataMapper
import com.payment.dto.PaymentDTO
import com.payment.dto.request.PaymentRequestDTOBuilder
import com.payment.service.PaymentMapper
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*
import static org.springframework.http.HttpStatus.*

class PaymentsControllerTest extends Specification  {
    def paymentMapper = new PaymentMapper()
    def paymentRequestDTOBuilder = new PaymentRequestDTOBuilder()
    def paymentDataMapper = Mock(PaymentDataMapper)
    def paymentController = new PaymentsController(
        paymentRequestDTOBuilder,
        paymentMapper,
        paymentDataMapper
    )

    MockMvc mockPaymentController = standaloneSetup(paymentController).build()

    def jsonOK ="{"+
        "\"amount\": \"100.21\"," +
        "\"currency\": \"GBP\"," +
        "\"reference\": \"debtor reference\"," +
        "\"end_to_end\": \"reference end-to-end\"," +
        "\"payment_type\": \"Credit\"," +
        "\"beneficiary_account\":{" +
        "\"account_name\":\"W Owens\"," +
        "\"account_number\":\"GB29XABC10161234567801\"," +
        "\"account_number_code\":\"IBAN\"" +
        "}," +
        "\"debtor_account\": {" +
        "\"account_name\": \"EJ Brown Black\"," +
        "\"account_number\": \"GB29XABC10161234567801\"," +
        "\"account_number_code\": \"IBAN\"" +
        "}" +
        "}"

    def jsonAmountMissing ="{"+
        "\"currency\": \"GBP\"," +
        "\"reference\": \"debtor reference\"," +
        "\"end_to_end\": \"reference end-to-end\"," +
        "\"payment_type\": \"Credit\"," +
        "\"beneficiary_account\":{" +
        "\"account_name\":\"W Owens\"," +
        "\"account_number\":\"GB29XABC10161234567801\"," +
        "\"account_number_code\":\"IBAN\"" +
        "}," +
        "\"debtor_account\": {" +
        "\"account_name\": \"EJ Brown Black\"," +
        "\"account_number\": \"GB29XABC10161234567801\"," +
        "\"account_number_code\": \"IBAN\"" +
        "}" +
        "}"


    def setup() {
    }


    def "postPayment test hits the URL and return HTTP created"() {
        when: 'created payment is hit'

        paymentDataMapper.savePayment(_ as PaymentDTO)>> true

        def response = mockPaymentController.perform(
            post('/payments').contentType(MediaType.APPLICATION_JSON).content(jsonOK)
        ).andReturn().response


        then:
        response.status == CREATED.value()
    }

    def "postPayment test hits the URL and empty request is sent"() {
        when: 'created payment is hit'

        def response = mockPaymentController.perform(
            post('/payments')
        ).andReturn().response
        then:
        response.status == BAD_REQUEST.value()
    }

    def "postPayment test hits the URL and one parameter is missing returned"() {
        when: 'created payment is hit'

        def response = mockPaymentController.perform(
            post('/payments').contentType(MediaType.APPLICATION_JSON).content(jsonAmountMissing)
        ).andReturn().response

        then:
        response.status == BAD_REQUEST.value()
    }
}
