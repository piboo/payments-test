package com.payment.dto.request

import com.payment.dto.request.PaymentRequestDTO
import com.payment.dto.request.PaymentRequestDTOBuilder
import com.payment.exception.PaymentsWrongParameterException
import spock.lang.Specification


class PaymentRequestDTOBuilderTest extends Specification {

    Map<String, Object> map = new HashMap()

    Map<String,String> mapAccount = new HashMap<>()

    def setup() {
        mapAccount.put("account_name", "name")
        mapAccount.put("account_number", "GB29XABC10161234567801")
        mapAccount.put("account_number_code", "IBAN")
    }

    def "Build correctly the object"() {
        when:
        map.put("amount","11")
        map.put("currency","GBP")
        map.put("reference","reference")
        map.put("end_to_end","end_to_end")
        map.put("payment_type","type")
        map.put("beneficiary_account", mapAccount)
        map.put("debtor_account", mapAccount)

        PaymentRequestDTOBuilder paymentRequestBuilder = new PaymentRequestDTOBuilder()
        PaymentRequestDTO paymentRequestDTO = paymentRequestBuilder.build(map)

        then:
        paymentRequestDTO.getAmount().getAmount() == Float.parseFloat("11")
        paymentRequestDTO.getAmount().getCurrency() == "GBP"
        paymentRequestDTO.getReference() == "reference"
        paymentRequestDTO.getReferenceE2E() == "end_to_end"
        paymentRequestDTO.getPaymentType() == "type"
    }

    def "it throws exception if one parameter is missing"() {
        when:
        map.put("amount","11")
        map.put("currency","GBP")
        map.put("reference","reference")
        map.put("end_to_end","end_to_end")
        map.put("payment_type","type")
        map.put("beneficiary_account", mapAccount)

        PaymentRequestDTOBuilder paymentRequestBuilder = new PaymentRequestDTOBuilder()

        paymentRequestBuilder.build(map)

        then:
        thrown PaymentsWrongParameterException
    }
}

