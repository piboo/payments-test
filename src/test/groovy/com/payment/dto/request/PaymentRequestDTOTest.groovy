package com.payment.dto.request

import com.payment.dto.request.PaymentRequestDTO
import com.payment.dto.request.valueObject.Account
import com.payment.dto.request.valueObject.Amount
import spock.lang.Specification

class PaymentRequestTest extends Specification  {
    def amount = new Amount("11.0","GBP")
    Account account

    def setup() {
        account = new Account(
            "name",
            "GB29XABC10161234567801",
            "IBAN"
        )
    }

    def "Return correctly the properties"() {
        when: 'a payment Request is created'
            def paymentRequest = new PaymentRequestDTO(
                amount,
                account,
                account,
                "reference",
                "reference-e2e",
                "paymentType",
            )
        then:
            paymentRequest.getAmount() == amount
            paymentRequest.getPaymentType() == "paymentType"
    }
}

