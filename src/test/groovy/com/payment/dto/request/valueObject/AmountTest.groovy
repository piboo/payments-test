package com.payment.dto.request.valueObject

import com.payment.dto.request.valueObject.Amount
import com.payment.exception.PaymentsWrongParameterException
import spock.lang.Specification

class AmountTest extends Specification  {

    def "Return correctly the properties"() {
        when: 'An Amount is created'
        def amount = new Amount("11.0","GBP")
        then:
        amount.getAmount() == Float.parseFloat("11.0")
        amount.getCurrency() == "GBP"
    }

    def "it throws exception if amount is accepted"() {
        when: 'An Amount is created'
        amount = new Amount("-1","GBP")

        then:
        thrown PaymentsWrongParameterException
    }

    def "it throws exception if amount is null"() {
        when: 'An Amount is created'
        amount = new Amount(null,"GBP")

        then:
        thrown PaymentsWrongParameterException
    }

    def "it throws exception if currency is accepted"() {
        when: 'An Amount is created'
        amount = new Amount("-1","GB")
        then:
        thrown PaymentsWrongParameterException
    }

    def "it throws exception if currency is null"() {
        when: 'An Amount is created'
        amount = new Amount("-1",null)
        then:
        thrown PaymentsWrongParameterException
    }
}

