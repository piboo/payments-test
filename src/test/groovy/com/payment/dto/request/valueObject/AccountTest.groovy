package com.payment.dto.request.valueObject

import com.payment.dto.request.valueObject.Account
import com.payment.exception.PaymentsWrongParameterException
import spock.lang.Specification


class AccountTest extends Specification {

    HashMap<String,String> map

    def setup() {
        map = new HashMap<String,String>()
        map.put("account_name", "name")
        map.put("account_number", "GB29XABC10161234567801")
        map.put("account_number_code", "IBAN")

    }

    def "Return correctly the properties"() {
        when: 'An Amount is created'
        def account = new Account(
            map.get("account_name"),
            map.get("account_number"),
            map.get("account_number_code")
        )

        then:
        account.getAccountName() == map.get("account_name")
        account.getAccountNumber() == map.get("account_number")
        account.getAccountNumberCode() == map.get("account_number_code")
    }

    def "it throws exception if property account_name is missing"() {
        when: 'An Amount is created'

        map.put("account_name",null)
        def account = new Account(
            map.get("account_name"),
            map.get("account_number"),
            map.get("account_number_code")
        )

        then:
        thrown PaymentsWrongParameterException
    }
}

